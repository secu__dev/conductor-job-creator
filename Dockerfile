FROM golang:alpine as build

LABEL maintainer="SecuDev <remy.chaix@gmail.com>"

WORKDIR $GOPATH/src/conductor-job-creator

COPY jobCreator.go .

RUN apk add --no-cache git mercurial \
 && go get -d -v ./... \
 && go install -v ./... \
 && apk del git mercurial

FROM alpine:latest

COPY --from=build /go/bin/conductor-job-creator /usr/local/bin/jobCreator

ENTRYPOINT ["/usr/local/bin/jobCreator"]