package main

import (
	"bytes"
	"encoding/json"
	"flag"
	"fmt"
	"io/ioutil"
	"net/http"
)

type Job struct {
	ClientType string   `json:"clientType"`
	Title      string   `json:"title"`
	Timeout    int      `json:"timeout"`
	Body       []string `json:"body"`
	Metrics    []string `json:"metrics"`
}

func main() {
	var file string
	var conductorServer string

	flag.StringVar(&file, "input", "input.json", "File listing job descriptions")
	flag.StringVar(&conductorServer, "server", "http://localhost:8181", "Conductor server url")
	flag.Parse()

	conductorApi := conductorServer + "/v1/jobs"

	data, err := ioutil.ReadFile(file)
	if err != nil {
		fmt.Println(err.Error())
		return
	}

	var listJobs []Job
	err = json.Unmarshal(data, &listJobs)
	if err == nil {
		// Successfully loaded the list of jobs, now to send it to the server
		resp, errPost := http.Post(conductorApi, "application/json", bytes.NewReader(data))
		if errPost != nil {
			fmt.Println(errPost.Error())
			return
		}
		defer resp.Body.Close()
		body, errBody := ioutil.ReadAll(resp.Body)
		if errBody == nil {
			fmt.Println(string(body))
		}
	}
}
